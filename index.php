<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Ninjas for Hire - Front-End Web and Mobile Developers (HTML, CSS, JavaScript, WordPress)</title>

		<meta name="description" content="Highly experienced and professional front-end web developers ensuring a smooth outsourcing experience. Specialising in HTML, CSS, JavaScript and WordPress.">

		<meta name="keywords" content="HTML, CSS, JavaScript, jQuery, WordPress, Outsource, Putsource partners, Outsource to South Africa, Outsource to Cape Town, Web development, Website development, Coders, Web coders, Mobile development">

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="SKYPE_TOOLBAR" CONTENT="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
		<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		<!--[if lt IE 7]><script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script><![endif]-->
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" href="css/main.css">

		<!-- (for production, also delete previous line) <link rel="stylesheet" href="dist/css/main.css"> -->
	</head>
	<body>
		<?php include "templates/nav.php"; ?>

		<?php include "templates/header.php"; ?>

		<?php include "templates/banner.php"; ?>

		<?php include "templates/about.php"; ?>

		<?php include "templates/our_work.php"; ?>

		<?php include "templates/clients.php"; ?>

		<?php include "templates/contact.php"; ?>
		<!-- start footer -->
		<div class="footer">
			<div class="row">
				&copy; Ninjas for Hire <?php echo date("Y"); ?>
			</div>
		</div>
		<script src="dist/js/scripts.js"></script>
		<script src="app.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
		<script>
		 WebFont.load({
		   google: {
		     families: ['Abril+Fatface', 'Alfa+Slab+One', 'Playfair+Display:400,400i', 'Roboto:300,400,500,700']
		   }
		 });
		</script>

		<script async type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-20953948-1']);
			_gaq.push(['_trackPageview']);
			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>

	</body>
</html>