<div id="contact" class="section contact">
	<div class="row alignnone">
		<h2><span>Work with the</span> Ninjas</h2>
		<p>Do you have a project you would like us to work on? Or perhaps a few questions?<br />
			Contact us and we will be more than happy to assist you.</p>

		<div class="contact-main">
			<div class="contact-left">
				<div class="ct-addy">
					<h4>Visit Us</h4>
					<p><strong>Ninjas for Hire</strong><br />
						<a href="tel:+27214800430">+27 21 480 0430</a><br />
						<a href="mailto:info@ninjasforhire.co.za">info@ninjasforhire.co.za</a><br />
						205 Masons Press<br />
						7 Ravenscraig Road<br />
						Woodstock<br />
						Cape Town<br />
						8000<br />
					</p>
				</div>
				<img src="images/contactbg3.jpg" alt="">
			</div>
			<div class="contact-right">
				<h4>Leave us a message</h4>
				<div id="form-messages"></div>
				<form id="ajax-contact" class="contact_form" method="post" action="emailaninja.php">
					<div class="input-required">
						<input id="firstname" name="firstname" placeholder="First Name" value="" type="text" required="">
					</div>
					
					<div class="input-required">
						<input id="telephone" name="telephone" placeholder="Phone Number" value="" type="text" required="">
					</div>

					<div class="input-required">
						<input id="email" name="email" placeholder="Email" value="" type="text" required="">
					</div>

					<textarea id="message" cols="1" rows="6" name="message" placeholder=" Your Message"></textarea>

					<input id="submit" type="submit" value="Submit Enquiry">
				</form>
			</div>
		</div>
	</div>
</div>