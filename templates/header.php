<div class="fixed_sides">
	<div class="row">
		<div class="side_left"><span class="spacer"></span><a href="#contact">Contact Us</a></div>
		<div class="side_right">
			<span class="spacer"></span>
			<a href="https://www.facebook.com/Ninjas-for-Hire-168474703234076/" target="_blank" rel="nofollow"><img class="sidefb" src="images/fb.svg"></a>
			<a href="https://twitter.com/ninjasforhire" target="_blank" rel="nofollow"><img class="sidetwit" src="images/twit.svg"></a>
			<span>Located in Sunny Cape Town, ZA</span>
		</div>
	</div>
</div>

<div class="fixed_top">
	<div class="row">
		<div class="header_contact">
			<div class="call">
				<img src="images/phone.png"> <a href="tel:+27214814900">+27(0)21 481 4900</a></div>
			<div class="email">
				<img src="images/mail.png"> <a href="mailto:info@ninjasforhire.co.za">info@ninjasforhire.co.za</a>
			</div>
		</div>
	</div>
</div>