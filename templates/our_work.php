<div id="work" class="section our_work">
	<div class="row">
		<h2>Some of Our <span>Work</span></h2>
		<div class="our-work-container">
			<div class="work-blurb">
				<h3>Weylandts<br /><span> Home</span></h3>
				<div class="project-location"><span><?php include "images/world.svg"; ?></span> Cape Town South Africa</div>
				<p>Welcome to the home of bold design with soul. Functional simplicity was the goal in this total overhaul of Weylandts online. The new site interfaces with store backend ERP Syspro for seamless stock integration, and is e-commerce ready - coming soon.</p>
				<div class="services-rendered">
					<span class="orange">What we did:</span>
					<ul>
						<li><?php include "images/wwd1.svg"; ?><div class="wwd">Digital and product development strategy</div></li>
						<li><?php include "images/wwd2.svg"; ?><div class="wwd">Analysis and product scoping</div></li>
						<li><?php include "images/wwd3.svg"; ?><div class="wwd">User Experience Design</div></li>
						<li><?php include "images/wwd4.svg"; ?><div class="wwd">User Interface Design</div></li>
						<li><?php include "images/wwd5.svg"; ?><div class="wwd">Front End Development</div></li>
						<li><?php include "images/wwd6.svg"; ?><div class="wwd">Custom Wordpress CMS Development</div></li>
					</ul>
				</div>
				<div class="view-site"><a href="https://www.weylandts.co.za" target="_blank" rel="nofollow">view site</a></div>
			</div>
			<div class="work-gallery">
				<div class="owl-carousel work-caro owl-theme">
					<div class="item"><img src="images/work/wey/img1.jpg"></div>
					<div class="item"><img src="images/work/wey/img2.jpg"></div>
					<div class="item"><img src="images/work/wey/img3.jpg"></div>
				</div>
			</div>
		</div>

		<div class="our-work-container">
			<div class="work-blurb">
				<h3>Dentsu Aegis <span>Network</span></h3>
				<div class="project-location"><span><?php include "images/world.svg"; ?></span> Cape Town South Africa</div>
				<p>The Dentsu Aegis Network is a global network of agencies that leverages its combined specialties in brand, media, digital, and creative communication to innovate in Africa. We built them a fresh take on portfolio sites, to showcase their well awarded advertising prowess.</p>
				<div class="services-rendered">
					<span class="orange">What we did:</span>
					<ul>
						<li><?php include "images/wwd1.svg"; ?><div class="wwd">Digital and product development strategy</div></li>
						<li><?php include "images/wwd5.svg"; ?><div class="wwd">Front End Development</div></li>
						<li><?php include "images/wwd6.svg"; ?><div class="wwd">Custom Wordpress CMS Development</div></li>
					</ul>
				</div>
				<div class="view-site"><a href="http://www.dentsuaegisnetwork.co.za" target="_blank" rel="nofollow">view site</a></div>
			</div>
			<div class="work-gallery">
				<div class="owl-carousel work-caro owl-theme">
					<div class="item"><img src="images/work/aegis/img1.jpg"></div>
					<div class="item"><img src="images/work/aegis/img2.jpg"></div>
					<div class="item"><img src="images/work/aegis/img3.jpg"></div>
				</div>
			</div>
		</div>

		<div class="our-work-container">
			<div class="work-blurb">
				<h3>Overland <span>Africa</span></h3>
				<div class="project-location"><span><?php include "images/world.svg"; ?></span> Cape Town, South Africa</div>
				<p>Africa, raw and rugged, in all its splendid beauty awaits. Take the road less travelled and venture off the beaten track with the experts at Overland Africa. We rebuilt this website with the traveller in mind, to simplify and inspire</p>
				<div class="services-rendered">
					<span class="orange">What we did:</span>
					<ul>
						<li><?php include "images/wwd2.svg"; ?><div class="wwd">Analysis and product scoping</div></li>
						<li><?php include "images/wwd3.svg"; ?><div class="wwd">User Experience Design</div></li>
						<li><?php include "images/wwd4.svg"; ?><div class="wwd">User Interface Design</div></li>
						<li><?php include "images/wwd5.svg"; ?><div class="wwd">Front End Development</div></li>
						<li><?php include "images/wwd6.svg"; ?><div class="wwd">Custom WordPress CMS Development</div></li>
					</ul>
				</div>
				<div class="view-site"><a href="https://www.overlandafrica.com" target="_blank" rel="nofollow">view site</a></div>
			</div>
			<div class="work-gallery">
				<div class="owl-carousel work-caro owl-theme">
					<div class="item"><img src="images/work/ola/img1.jpg"></div>
					<div class="item"><img src="images/work/ola/img2.jpg"></div>
					<div class="item"><img src="images/work/ola/img3.jpg"></div>
				</div>
			</div>
		</div>

		<div class="our-work-container">
			<div class="work-blurb">
				<h3>Aero<span>FS</span></h3>
				<div class="project-location"><span><?php include "images/world.svg"; ?></span> Palo Alto, California, USA</div>
				<p>File sharing start-up, AeroFS took Silicone Valley by storm in early 2010, and have gone from strength to strength ever since. We maintain their WPEngine hosted WordPress site and make sure it is the well oiled machine it should be</p>
				<div class="services-rendered">
					<span class="orange">What we did:</span>
					<ul>
						<li><?php include "images/wwd4.svg"; ?><div class="wwd">User Interface Design</div></li>
						<li><?php include "images/wwd5.svg"; ?><div class="wwd">Front End Development</div></li>
						<li><?php include "images/wwd6.svg"; ?><div class="wwd">Custom WordPress CMS Development</div></li>
					</ul>
				</div>
				<div class="view-site"><a href="https://www.aerofs.com" target="_blank" rel="nofollow">view site</a></div>
			</div>
			<div class="work-gallery">
				<div class="owl-carousel work-caro owl-theme ">
					<div class="item"><img src="images/work/aero/img1.jpg"></div>
					<div class="item"><img src="images/work/aero/img2.jpg"></div>
					<div class="item"><img src="images/work/aero/img3.jpg"></div>
				</div>
			</div>
		</div>

		<div class="our-work-container">
			<div class="work-blurb">
				<h3>Cape <span>Brandy</span></h3>
				<div class="project-location"><span><?php include "images/world.svg"; ?></span> Grabouw, South Africa</div>
				<p>2017 saw the launch of a brandy collective to rival Cognac's global domination of the oldest and most noble spirit. A fresh, modern and light website was needed to reach and educate the modern craft spirits drinker.</p>
				<div class="services-rendered">
					<span class="orange">What we did:</span>
					<ul>
						<li><?php include "images/wwd2.svg"; ?><div class="wwd">Analysis and product scoping</div></li>
						<li><?php include "images/wwd3.svg"; ?><div class="wwd">User Experience Design</div></li>
						<li><?php include "images/wwd4.svg"; ?><div class="wwd">User Interface Design</div></li>
						<li><?php include "images/wwd5.svg"; ?><div class="wwd">Front End Development</div></li>
						<li><?php include "images/wwd6.svg"; ?><div class="wwd">Custom WordPress CMS Development</div></li>
					</ul>
				</div>
				<div class="view-site"><a href="http://capebrandy.org" target="_blank" rel="nofollow">view site</a></div>
			</div>
			<div class="work-gallery">
				<div class="owl-carousel work-caro owl-theme ">
					<div class="item"><img src="images/work/cb/img1.jpg"></div>
					<div class="item"><img src="images/work/cb/img2.jpg"></div>
					<div class="item"><img src="images/work/cb/img3.jpg"></div>
				</div>
			</div>
		</div>

		<div class="our-work-container">
			<div class="work-blurb">
				<h3>The Franschhoek <span>Cellar</span></h3>
				<div class="project-location"><span><?php include "images/world.svg"; ?></span> Franschhoek, South Africa</div>
				<p>The Franschhoek Cellar is a beautiful asset to the Franschhoek wine route. Our challenge with this site was to identify and direct different user journey's through what is a multi functional premium venue</p>
				<div class="services-rendered">
					<span class="orange">What we did:</span>
					<ul>
						<li><?php include "images/wwd2.svg"; ?><div class="wwd">Analysis and product scoping</div></li>
						<li><?php include "images/wwd3.svg"; ?><div class="wwd">User Experience Design</div></li>
						<li><?php include "images/wwd4.svg"; ?><div class="wwd">User Interface Design</div></li>
						<li><?php include "images/wwd5.svg"; ?><div class="wwd">Front End Development</div></li>
						<li><?php include "images/wwd6.svg"; ?><div class="wwd">Custom WordPress CMS Development</div></li>
					</ul>
				</div>
				<div class="view-site"><a href="http://thefranschhoekcellar.co.za" target="_blank" rel="nofollow">view site</a></div>
			</div>
			<div class="work-gallery">
				<div class="owl-carousel work-caro owl-theme ">
					<div class="item"><img src="images/work/fhc/img1.jpg"></div>
					<div class="item"><img src="images/work/fhc/img2.jpg"></div>
					<div class="item"><img src="images/work/fhc/img3.jpg"></div>
				</div>
			</div>
		</div>

		<div class="our-work-container">
			<div class="work-blurb">
				<h3>Soft Light <span>City</span></h3>
				<div class="project-location"><span><?php include "images/world.svg"; ?></span> London, United Kingdom</div>
				<p>Soft Light City do Original Music, Production Music, Sound Design and Final Mix. They like to let the work speak for itself, so we built them a portfolio site to do just this, show off their work in the best possible manner, with as little distraction as possible.</p>
				<div class="services-rendered">
					<span class="orange">What we did:</span>
					<ul>
						<li><?php include "images/wwd2.svg"; ?><div class="wwd">Analysis and product scoping</div></li>
						<li><?php include "images/wwd3.svg"; ?><div class="wwd">User Experience Design</div></li>
						<li><?php include "images/wwd4.svg"; ?><div class="wwd">User Interface Design</div></li>
						<li><?php include "images/wwd5.svg"; ?><div class="wwd">Front End Development</div></li>
						<li><?php include "images/wwd6.svg"; ?><div class="wwd">Custom Wordpress CMS Development</div></li>
					</ul>
				</div>
				<div class="view-site"><a href="http://www.softlightcity.com" target="_blank" rel="nofollow">view site</a></div>
			</div>
			<div class="work-gallery">
				<div class="owl-carousel work-caro owl-theme ">
					<div class="item"><img src="images/work/slc/img1.jpg"></div>
					<div class="item"><img src="images/work/slc/img2.jpg"></div>
				</div>
			</div>
		</div>
	</div>
</div>







