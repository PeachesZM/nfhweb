<div class="nav_menu">
	<nav class="main_menu">
		<ul>
			<li><a class="menu_item" href="#">Home</a></li>
			<li><a class="menu_item" href="#about">About Us</a></li>
			<li><a class="menu_item" href="#work">Our Work</a></li>
			<li><a class="menu_item" href="#contact">Contact Us</a></li>
		</ul>
	</nav>
	<!-- Site Overlay -->
	<div class="site-overlay"></div>
</div>