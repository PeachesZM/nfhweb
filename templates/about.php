<div id="about" class="section about">
	<div class="row">
		<div class="about-left">
			<h2><span>we are</span><br> Web Developers</h2>
			<p>Beside being masters in HTML, CSS, JS and WordPress, we're also pretty good problem solvers, User Experience and Interface designers. Web all-
			rounders if you like, but at our core is development. No amount of fancy photoshop footwork,
			or user tested wireframery will ever make up for something that is badly built on the web.</p>

			<p>And that’s something we take to heart. Apart from working with our own local and overseas
			client base, we're fully able to service other digital and traditional agencies as reliable
			overflow partners. </p>
			<p>That’s where the business started, and still what we enjoy doing - bringing varied visions and
			strategies to life on the web. We’ve been there, done that and got the black belt with agencies and SMEs
			- rest assured you'll be in good hands with us. </p>
			<p>Have a look at some of our own work, or jump to the list of quality companies we've worked
			with then get in touch for a chat. </p>
			<a href="#contact">We'd love to hear from you.</a>
		</div>
		<div class="about-right">
			<div class="title-wrapper">
				<h3>our</h3>
				<h2>Service<br />Offering</h2>
			</div>
			<ul>
				<li>
					<span class="service-icon">
						<?php include "images/so1.svg"; ?>
					</span><span class="service-text">Digital and product <br />development strategy</span>
				</li>
				<li>
					<span class="service-icon">
						<?php include "images/so2.svg"; ?>
					</span><span class="service-text">User Interface <br />Design</span>
				</li>
				<li>
					<span class="service-icon">
						<?php include "images/so3.svg"; ?>
					</span><span class="service-text">Analysis and <br />product scoping</span>
				</li>
				<li>
					<span class="service-icon">
						<?php include "images/so4.svg"; ?>
					</span><span class="service-text">Front End <br />Development</span>
				</li>
				<li>
					<span class="service-icon">
						<?php include "images/so5.svg"; ?>
					</span><span class="service-text">User Experience <br />Design</span>
				</li>
				<li>
					<span class="service-icon">
						<?php include "images/so6.svg"; ?>
					</span><span class="service-text">Custom Wordpress <br />CMS Development</span>
				</li>
			</ul>
		</div>
	</div>
</div>