<div class="section clients">
	<div class="row">
		<h2>World class companies <span>we have worked with</span> </h2>
		<div class="clients-main">
			<span class="client"><?php include "images/clients/1.svg"; ?></span>
			<span class="client"><?php include "images/clients/2.svg"; ?></span>
			<span class="client"><?php include "images/clients/3.svg"; ?></span>
			<span class="client"><?php include "images/clients/4.svg"; ?></span>
			<span class="client"><?php include "images/clients/5.svg"; ?></span>
			<span class="client"><?php include "images/clients/6.svg"; ?></span>
			<span class="client"><?php include "images/clients/7.svg"; ?></span>
			<span class="client"><?php include "images/clients/8.svg"; ?></span>
			<span class="client"><?php include "images/clients/9.svg"; ?></span>
			<span class="client"><?php include "images/clients/10.svg"; ?></span>
			<span class="client"><?php include "images/clients/11.svg"; ?></span>
			<span class="client"><?php include "images/clients/12.svg"; ?></span>
			<span class="client"><?php include "images/clients/13.svg"; ?></span>
			<span class="client"><?php include "images/clients/14.svg"; ?></span>
			<span class="client"><?php include "images/clients/15.svg"; ?></span>
			<span class="client"><?php include "images/clients/16.svg"; ?></span>
			<span class="client"><?php include "images/clients/17.svg"; ?></span>
			<span class="client"><?php include "images/clients/18.svg"; ?></span>
			<span class="client"><?php include "images/clients/19.svg"; ?></span>
			<span class="client"><?php include "images/clients/20.svg"; ?></span>
			<span class="client"><?php include "images/clients/21.svg"; ?></span>
			<span class="client"><?php include "images/clients/23.svg"; ?></span>
			<span class="client"><?php include "images/clients/24.svg"; ?></span>
			<span class="client"><?php include "images/clients/25.svg"; ?></span>
			<span class="client"><?php include "images/clients/26.svg"; ?></span>
			<span class="client"><?php include "images/clients/27.svg"; ?></span>
			<span class="client"><?php include "images/clients/28.svg"; ?></span>
			<span class="client"><?php include "images/clients/29.svg"; ?></span>
			<span class="client"><?php include "images/clients/30.svg"; ?></span>
			<span class="client"><?php include "images/clients/31.svg"; ?></span>
		</div>
	</div>
</div>
