<div class="section banner">
	<div class="row">
		<div class="site_logo">
			<div class="logo"><a href="/"><img src="images/logo.svg" alt=""></a></div>
			<div id="btn"><span></span><span></span><span></span></div>
		</div>
		<div class="banner_main">
			<div class="site_intro_small">Specialists in the mystic art of quick and deadly</div>
			<h1 class="site_intro_big">HTML, CSS, JavaScript <br /><span>&</span> WordPress.</h1>
		</div>
	</div>
</div>